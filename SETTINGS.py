import os

USER_DATA_FILEPATH = 'fbuser.dat'

if os.path.isfile(USER_DATA_FILEPATH):
    with open(USER_DATA_FILEPATH, 'r') as fin:
        lines = fin.readlines()
        USER_EMAIL = lines[0]
        USER_PASSWORD = lines[1]

COOKIE_FILEPATH = 'cookie'

PROBLEM_CACHE_FILEPATH = 'cache_problems.json'
CONTEST_CACHE_FILEPATH = 'cache_contests.json'
SUBSCRIBERS_FILEPATH = 'subscribers.json'

COMMAND_PREFIX = '!'