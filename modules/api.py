import requests
import json
import base64
import os

class Api():
    def __init__(self, sessionid):
        self.sessionid = sessionid

        self.session = requests.Session()
        self.session.cookies.set("sessionid", self.sessionid)

    def get_contests(self):
        r = self.session.get('http://bajton.vlo.gda.pl/api/contests')
        contests = json.loads(r.text)["data"]["results"]

        # add url
        for con_i in range(len(contests)):
            contests[con_i]["url"] = 'http://bajton.vlo.gda.pl/contest/' + str(contests[con_i]["id"])

        return contests

    def get_problems(self, contest_id):
        if not isinstance(contest_id, str):
            contest_id = str(contest_id)
        r = self.session.get('http://bajton.vlo.gda.pl/api/contest/problem?contest_id=' + contest_id)
        problems = json.loads(r.text)["data"]

        # check if password is not entered
        if isinstance(problems, str):
            return []

        # add url
        for pro_i in range(len(problems)):
            problems[pro_i]["url"] = 'http://bajton.vlo.gda.pl/contest/' + contest_id + '/problem/' + problems[pro_i]["_id"].replace(' ', '%20')

        return problems

    def get_public_problems(self):
        r = self.session.get('http://bajton.vlo.gda.pl/api/problem?limit=1000')
        problems = json.loads(r.text)["data"]["results"]

        # add url
        for pro_i in range(len(problems)):
            problems[pro_i]["url"] = 'http://bajton.vlo.gda.pl/problem/' + problems[pro_i]["_id"].replace(' ', '%20')

        return problems


def load_api_from_file(filename):
    if not os.path.isfile(filename):
        # get new sessionid
        cookie = {'sessionid': input("Session id: ")}
        # save data
        with open(filename, 'wb') as fout:
            fout.write(base64.encodebytes(json.dumps(cookie).encode()))
    else:
        # read previously saved cookie
        with open(filename, 'rb') as fin:
            cookie = json.loads(base64.decodebytes(fin.read()).decode())

    return Api(cookie['sessionid'])