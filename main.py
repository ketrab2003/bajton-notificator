from fbchat import Client, ThreadType, Message, MessageReaction
import asyncio
import modules.api as bajton_api
import os
import sys
import json
import SETTINGS

### CHECK IF FACEBOOK ACCOUNT DATA IS SET ###
try:
    _ = SETTINGS.USER_EMAIL
    _ = SETTINGS.USER_PASSWORD
except:
    print("Facebook account data missing :/\nPerhaps you need to run configure.py first")
    sys.exit()

# import languages
import localisation.lan_pl as loc

def load_json(filename):
    data = []
    if os.path.isfile(filename):
        with open(filename, 'r') as f:
            data = json.loads(f.read())
    return data

def save_json(filename, data):
    with open(filename, 'w+') as f:
        f.write(json.dumps(data))

# changing thread type to storeable data
int_to_threadtype = {0: ThreadType.USER,
                     1: ThreadType.GROUP,
                     2: ThreadType.MARKETPLACE,
                     3: ThreadType.PAGE}

def type_to_int(thread_type):
    if thread_type == ThreadType.USER:
        return 0
    if thread_type == ThreadType.GROUP:
        return 1
    if thread_type == ThreadType.MARKETPLACE:
        return 2
    if thread_type == ThreadType.PAGE:
        return 3

def int_to_type(thread_type):
    return int_to_threadtype[thread_type]

### set commands for bot ###
async def subscribe(client, mid=None, author_id=None, message_object=None, thread_id=None,
              thread_type=ThreadType.USER, at=None, metadata=None, msg=None):
    """ Subscribe thread to send Bajton notifications """
    # load subscribers
    subs = load_json(SETTINGS.SUBSCRIBERS_FILEPATH)

    # generate profile
    new_sub = {'id': thread_id, 'type': type_to_int(thread_type)}

    # if already subscribed
    if new_sub in subs:
        await client.send(Message(text=loc.generate_already_subbed()), thread_id=thread_id, thread_type=thread_type)
        return

    subs.append(new_sub)
    await client.react_to_message(mid, MessageReaction.HEART)
    await client.send(Message(text=loc.generate_subscribe_thanks()), thread_id=thread_id, thread_type=thread_type)

    save_json(SETTINGS.SUBSCRIBERS_FILEPATH, subs)

async def info(client, mid=None, author_id=None, message_object=None, thread_id=None,
              thread_type=ThreadType.USER, at=None, metadata=None, msg=None):
    """ Send info about project """
    await client.send(Message(text=loc.generate_info()), thread_id=thread_id, thread_type=thread_type)

async def help(client, mid=None, author_id=None, message_object=None, thread_id=None,
              thread_type=ThreadType.USER, at=None, metadata=None, msg=None):
    """ Send info about commands """
    await client.send(Message(text=loc.generate_help(SETTINGS.COMMAND_PREFIX)), thread_id=thread_id, thread_type=thread_type)

commands = {'subscribe': subscribe,
            'info': info,
            'help': help,}

# Subclass fbchat.Client and override required methods
class EchoBot(Client):
    async def on_message(self, mid=None, author_id=None, message_object=None, thread_id=None,
                         thread_type=ThreadType.USER, at=None, metadata=None, msg=None):
        await self.mark_as_delivered(thread_id, message_object.uid)
        await self.mark_as_read(thread_id)

        #print(ord(message_object.text[0]))

        # If you're not the author and it is command, react
        if author_id != self.uid and message_object.text.startswith(SETTINGS.COMMAND_PREFIX):
            command = message_object.text[len(SETTINGS.COMMAND_PREFIX):].lower()
            if command in commands.keys():
                await commands[command](self, mid, author_id, message_object, thread_id,
                                        thread_type, at, metadata, msg)


#### sending notification from Bajton ###

def get_problems_delta(api, cache_file):
    # try to load cache
    cache = load_json(cache_file)

    # load all problems
    problems = api.get_public_problems()
    contsets = api.get_contests()
    for contest in contsets:
        if int(contest['status']) > -1:
            # if contest active - fetch problems
            problems.extend(api.get_problems(contest['id']))

    # cache new problems
    save_json(cache_file, problems)

    # compare data and extract delta
    delta = [problem for problem in problems if not any([problem['id'] == prob['id'] for prob in cache])]

    return delta

def get_contests_delta(api, cache_file):
    # try to load cache
    cache = load_json(cache_file)

    # load new contests
    new = api.get_contests()

    # cache new contests
    save_json(cache_file, new)

    # compare data and extract delta
    delta = [contest for contest in new if not any([contest['id'] == con['id'] for con in cache])]

    return delta

async def check_and_notify(client, api):
    # get subscribers
    subs = load_json(SETTINGS.SUBSCRIBERS_FILEPATH)

    delta = get_contests_delta(api, SETTINGS.CONTEST_CACHE_FILEPATH)
    for contest in delta:
        for sub in subs:
            await client.send(Message(text=loc.generate_new_contest_info(contest)), thread_id=sub['id'], thread_type=int_to_type(sub['type']))

    delta = get_problems_delta(api, SETTINGS.PROBLEM_CACHE_FILEPATH)
    for problem in delta:
        for sub in subs:
            await client.send(Message(text=loc.generate_new_problem_info(problem)), thread_id=sub['id'], thread_type=int_to_type(sub['type']))

async def keep_checking(client):
    api = bajton_api.load_api_from_file(SETTINGS.COOKIE_FILEPATH)

    # Init cache download
    save_json(SETTINGS.CONTEST_CACHE_FILEPATH, api.get_contests())
    problems = api.get_public_problems()
    contsets = api.get_contests()
    for contest in contsets:
        if int(contest['status']) > -1:
            # if contest active - fetch problems
            problems.extend(api.get_problems(contest['id']))
    save_json(SETTINGS.PROBLEM_CACHE_FILEPATH, problems)

    while 1:
        await check_and_notify(client, api)
        await asyncio.sleep(10)

### setting up loop for bot ###
loop = asyncio.get_event_loop()

async def start():
    client = EchoBot(loop=loop)
    await client.start(SETTINGS.USER_EMAIL, SETTINGS.USER_PASSWORD)
    if await client.is_logged_in():
        print("Logged in!")
    client.listen()

    # start checking for new Bajtons
    await keep_checking(client)


loop.run_until_complete(start())
loop.run_forever()