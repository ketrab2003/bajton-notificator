import SETTINGS
import base64
import json

# get new sessionid
cookie = {'sessionid': input("Session id: ")}
# save data
with open(SETTINGS.COOKIE_FILEPATH, 'wb') as fout:
    fout.write(base64.encodebytes(json.dumps(cookie).encode()))

# get facebook user data
with open(SETTINGS.USER_DATA_FILEPATH, 'w+') as fout:
    fout.write(input('Facebook user email: ') + '\n')
    fout.write(input('Facebook user password: ') + '\n')