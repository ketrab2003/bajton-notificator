def generate_new_contest_info(contest):
    return f"New contest avaliable!\n| {contest['title']} |\n{contest['url']}"

def generate_new_problem_info(problem):
    return f"New problem avaliable!\n| {problem['title']} |\n{problem['url']}"

def generate_already_subbed():
    return "This thread is already subscribed."

def generate_subscribe_thanks():
    return "From now on you will be getting notifications about new problems and contests from Bajton."