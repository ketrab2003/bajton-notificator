def generate_new_contest_info(contest):
    message = chr(127942) + f"Dostępny jest nowy konkurs!\n\n  {contest['title']}  \n\n{'-'*25}\n{contest['url']}\n"
    if contest['contest_type'] == 'Password Protected':
        message += chr(128272)
    return message

def generate_new_problem_info(problem):
    message = chr(128234) + f"Dostępne jest nowe zadanie!\n\n  {problem['title']}  \n\n{'-'*25}\n{problem['url']}\n"
    return message

def generate_already_subbed():
    return "Ten wątek jest już zasubskrybowany. " + chr(10084)

def generate_subscribe_thanks():
    return chr(127881) + " Od teraz będziesz otrzymywał powiadomienia o nowych konkursach i zadaniach z Bajtona. " + chr(127881)

def generate_info():
    return "Projekt powstał w celu sprawdzania i informowania o nowych zadaniach na Bajtonie (http://bajton.vlo.gda.pl/)\nAutorzy:\n - Bartek K - @ketrab2003\nStrona projektu na Gitlabie:\nhttps://gitlab.com/ketrab2003/bajton-notificator\nProjekt jest rozprowadzny na zasadach licencji MIT"

def generate_help(prefix):
    return f"Podstawowe komendy:\n {prefix}help - wyświetla to okno pomocy\n {prefix}subscribe - dodaj swoją grupę aby otrzymywać informacje o nowych zadaniach\n {prefix}info - wyświetl informacje o projekcie"
